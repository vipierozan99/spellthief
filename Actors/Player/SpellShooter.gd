extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var Spell = preload("res://Actors/Spell/Spell.tscn")
onready var Enemy = preload("res://Actors/Enemy/Enemy.tscn")
onready var worldNode = get_node("/root/World")
onready var playerNode = get_parent()
onready var reticleNode = get_node("/root/World/Reticle")

onready var cooldownTimer = Timer.new()

var canCast = true
var spellCooldown = .25



# Called when the node enters the scene tree for the first time.
func _ready():
	cooldownTimer.set_one_shot(true)
	cooldownTimer.set_wait_time(spellCooldown)
	cooldownTimer.connect("timeout",self,"on_timeout")
	add_child(cooldownTimer)
	

func on_timeout():
	canCast = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("primary") and canCast:
		canCast = false
		cooldownTimer.start()
		var spell_direction = (reticleNode.get_position()-playerNode.get_position()).normalized()
		var spell_spawn_pos = playerNode.get_position() + spell_direction * 30
		var spell_instance = Spell.instance().init(spell_spawn_pos,spell_direction,"Fireball", "Player")
		worldNode.add_child(spell_instance)
	if Input.is_action_pressed("secondary") and canCast:
		canCast = false
		cooldownTimer.start()
		var enemy_instance = Enemy.instance()
		enemy_instance.set_position(reticleNode.get_position())
		
		worldNode.add_child(enemy_instance)
		


