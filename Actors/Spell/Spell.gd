extends KinematicBody2D


const SPELL_SPEED = 12

var linear_vel = Vector2()

func set_collision_by_caster(caster: String):
	match caster:
		"Player":
			set_collision_mask_bit(0,true)

func init(position, direction, type, caster):
	set_position(position)
	look_at(position + direction)
	linear_vel = direction * SPELL_SPEED
	set_name("Spell_"+type)
	set_collision_by_caster(caster)
	return self

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var collision = move_and_collide(linear_vel)
	if(collision):
		var collider = collision.get_collider()
		if(collider.has_method("on_projectile_collide")):
			collider.on_projectile_collide(self,collision)
		queue_free()
