extends Camera2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var PlayerNode = get_node("/root/World/Player")
	var ReticleNode = get_node("/root/World/Reticle")
	var playerToReticleVec = ReticleNode.get_position() - PlayerNode.get_position()
	playerToReticleVec = playerToReticleVec / 8
	set_offset(playerToReticleVec)
